package getRequest;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class GetData {
	
   //TestNG Test Framework - Test Case Rest API:AC1 with Data parameterized using DataSet - dataSetLatLon defined below the test cases
	@Test(dataProvider="dataSetLatLon")
	public void testDataLatLon(double Lat,double Lon, String City)
	{
	     //Get Request Input is constructed by using the Latitude(Lat) and Longitude(Lon) parameter inputs from DataSet
		String req1 = "https://api.weatherbit.io/v2.0/current?lat="+Lat+"&lon="+Lon+"&key=33af0fba989444749bbce2e015beb0f4";
		Response resp=RestAssured.get(req1);
		int code=resp.getStatusCode();
		System.out.println("Status code is "+code);
		Assert.assertEquals(code,200);
		String data=resp.asString();
		System.out.println("Weather Data for city: "+City+" by passing Latitude and Longitude as inputs is"+data);	
	}
	
	//TestNG Test Framework - Test Case Rest API:AC2 with Data parameterized using DataSet - dataSetLatLon defined below the test cases
	@Test(dataProvider="dataSetPostCode")
	public void testDataLatLon(int PostCode, String City)
	{
		//Get Request Input is constructed by using the PostalCode parameter inputs from DataSet
		String req1 = "https://api.weatherbit.io/v2.0/current?postal_code="+PostCode+"&key=33af0fba989444749bbce2e015beb0f4";
		Response resp=RestAssured.get(req1);
		int code=resp.getStatusCode();
		System.out.println("Status code is "+code);
		Assert.assertEquals(code,200);
		String data=resp.asString();
		System.out.println("Weather Data for city: "+City+" by passing PostCode as inputs is"+data);	
	}
	
	//DataSet for AC1 - 5 sets of Data (Lat & Lon)
	@DataProvider
	public Object[][] dataSetLatLon()
	{
		//Row stands for how many different data types  test should run
		//Column stands for how many values per each test
		Object[][] data=new Object[5][3];
		//1st row  1st column
		data[0][0]=-33.86785;
		//1st row  2nd column
		data[0][1]=151.20732;
		//1st row  3rd column
		data[0][2]="Sydney";
		data[1][0]=-37.814;
		data[1][1]=144.96332;
		data[1][2]="Melbourne";
		data[2][0]=-31.95224;
		data[2][1]=115.8614;
		data[2][2]="Perth";
		data[3][0]=-34.92866;
		data[3][1]=138.59863;
		data[3][2]="Adelaide";
		data[4][0]=-27.46794;
		data[4][1]=153.02809;
		data[4][2]="Brisbane";
		return data;	
	}
	
	//DataSet for AC2 - 5 sets of Data (PostalCode)
	@DataProvider
	public Object[][] dataSetPostCode()
	{
		//Row stands for how many different data types  test should run
	    //Column stands for how many values per each test
		Object[][] data=new Object[5][2];
		//1st Row 1st column
		data[0][0]=03431;
		//1st Row 2nd column
		data[0][1]="Cheshire";
		data[1][0]=60007;
		data[1][1]="Cook";
		data[2][0]=80201;
		data[2][1]="Denver";
		data[3][0]=95834;
		data[3][1]="Sacramento";
		data[4][0]=80907;
		data[4][1]="El Paso";
		return data;	
	}
}
