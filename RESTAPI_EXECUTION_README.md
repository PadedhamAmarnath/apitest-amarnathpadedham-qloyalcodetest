
# Instructions for Execution of REST API Challenge - Role Applied - Automation Engineer

# Framework
This REST API Test is Automated using Rest Assured on Eclipse IDE usig Maven Project, TestNG Framework.

#Libraries/Files Uploaded to the branch
1.Maven project folder 'restAssuredFramework' with all the source files including the POM.xml, Rest Assured dependencies are added.
2. RESTAPI_EXECUTION_README.md file (this file) with execution instructions

#Prerequisites
1. Java,Selenium WebDriver with Java, Maven, Eclipse IDE (or similar) installed/configured.

#Test Design
1. Both the Test cases are added to Java Class File - src/test/java/GetWeatherData.java using TestNG framework.
2. Test Case 1 covers the Test AC1 - Get Current Weather Data for multiple places based on Lat and Lon.
   a. Input String for RestAssured.get command is constructed by passing Lat, Lon and the City name from Dataset
   (5 sets of data are passed from dataSetLatLon).
3. Test Case 2 covers the Test AC2 - Get current weather data for multiple places based on Postal Code.
   a. Input string for RestAssured.get command is constructed by passing PostalCode and the City name from Dataset
   (5 sets of data are passed from dataSetPostCode).


#Execution Steps
1. Import the Maven Project from Eclipse 

2. Select src/test/java/GetWeatherData.java Test and run it as an TestNG.

3. Test should pass without errors by each Test case executing with 5 different sets of Data.

4. TestCase1 Test Results : are logged by displaying:
   a.  the Status Code as 200 
   b.  the weather data by displaying <expected City>, input parameters <Lat>,<Lon> followed by <response data>

5. TestCase2 Test Results : are logged by displaying:
   a.  the Status Code as 200 
   b.  the weather data by displaying <expected City>, input parameter <potal_code>  followed by <response data>

6. Expected Result to be displayed is as below:
Status code is 200
Weather Data for city: Sydney by passing Latitude and Longitude as inputs is{"data":[{"wind_cdir":"ESE","rh":66,"pod":"n","pres":1018.3,"timezone":"Australia\/Sydney","ob_time":"2019-04-01 13:40","country_code":"AU","clouds":0,"ts":1554126000,"solar_rad":0,"state_code":"02","lat":-33.87,"wind_spd":2.68,"wind_cdir_full":"east-southeast","slp":1026.9,"vis":10,"lon":151.21,"uv":0,"datetime":"2019-04-01:13","h_angle":-90,"dewpt":12.9,"aqi":27,"dhi":0,"wind_dir":103,"elev_angle":-57.92,"ghi":0,"precip":null,"sunrise":"20:07","city_name":"Sydney","weather":{"icon":"c01n","code":"800","description":"Clear sky"},"sunset":"07:50","temp":19.4,"dni":0,"station":"E8081","app_temp":19.2}],"count":1}

Status code is 200
Weather Data for city: Melbourne by passing Latitude and Longitude as inputs is{"data":[{"wind_cdir":"SE","rh":81,"pod":"n","pres":1026.7,"timezone":"Australia\/Melbourne","ob_time":"2019-04-01 13:38","country_code":"AU","clouds":0,"ts":1554125880,"solar_rad":0,"state_code":"07","lat":-37.81,"wind_spd":1,"wind_cdir_full":"southeast","slp":1027.4,"vis":10,"lon":144.96,"uv":0,"datetime":"2019-04-01:13","h_angle":-90,"dewpt":8,"aqi":26,"dhi":0,"wind_dir":133,"elev_angle":-51.88,"ghi":0,"precip":null,"sunrise":"20:33","city_name":"Melbourne","weather":{"icon":"c01n","code":"800","description":"Clear sky"},"sunset":"08:13","temp":11.1,"dni":0,"station":"E5657","app_temp":11.2}],"count":1}

Status code is 200
Weather Data for city: Perth by passing Latitude and Longitude as inputs is{"data":[{"wind_cdir":"NNW","rh":100,"pod":"n","pres":1015.5,"timezone":"Australia\/Perth","ob_time":"2019-04-01 13:40","country_code":"AU","clouds":0,"ts":1554126000,"solar_rad":0,"state_code":"08","lat":-31.95,"wind_spd":1.79,"wind_cdir_full":"north-northwest","slp":1024.4,"vis":10,"lon":115.86,"uv":0,"datetime":"2019-04-01:13","h_angle":-90,"dewpt":21.7,"aqi":33,"dhi":0,"wind_dir":330,"elev_angle":-35.98,"ghi":0,"precip":null,"sunrise":"22:27","city_name":"Perth","weather":{"icon":"c01n","code":"800","description":"Clear sky"},"sunset":"10:12","temp":21.7,"dni":0,"station":"AT723","app_temp":22.5}],"count":1}

Status code is 200
Weather Data for city: Adelaide by passing Latitude and Longitude as inputs is{"data":[{"wind_cdir":"ESE","rh":76,"pod":"n","pres":1019.5,"timezone":"Australia\/Adelaide","ob_time":"2019-04-01 13:40","country_code":"AU","clouds":0,"ts":1554126000,"solar_rad":0,"state_code":"05","lat":-34.93,"wind_spd":1,"wind_cdir_full":"east-southeast","slp":1025.3,"vis":10,"lon":138.6,"uv":0,"datetime":"2019-04-01:13","h_angle":-90,"dewpt":10.8,"aqi":9,"dhi":0,"wind_dir":104,"elev_angle":-50.73,"ghi":0,"precip":null,"sunrise":"20:58","city_name":"Adelaide","weather":{"icon":"c01n","code":"800","description":"Clear sky"},"sunset":"08:40","temp":15,"dni":0,"station":"D0156","app_temp":15}],"count":1}

Status code is 200
Weather Data for city: Brisbane by passing Latitude and Longitude as inputs is{"data":[{"wind_cdir":"NW","rh":78,"pod":"n","pres":1017.8,"timezone":"Australia\/Brisbane","ob_time":"2019-04-01 13:36","country_code":"AU","clouds":0,"ts":1554125760,"solar_rad":0,"state_code":"04","lat":-27.47,"wind_spd":1,"wind_cdir_full":"northwest","slp":1020.4,"vis":10,"lon":153.03,"uv":0,"datetime":"2019-04-01:13","h_angle":-90,"dewpt":13.9,"aqi":20,"dhi":0,"wind_dir":313,"elev_angle":-64.23,"ghi":0,"precip":null,"sunrise":"19:57","city_name":"Brisbane","weather":{"icon":"c01n","code":"800","description":"Clear sky"},"sunset":"07:45","temp":17.8,"dni":0,"station":"F1718","app_temp":17.8}],"count":1}

Status code is 200
Weather Data for city: Cheshire by passing PostCode as inputs is{"data":[{"rh":40,"pod":"d","lon":"6.9039","pres":967.9,"timezone":"Europe\/Zurich","ob_time":"2019-04-01 13:40","country_code":"CH","clouds":100,"ts":1554126000,"solar_rad":144.2,"state_code":"VD","lat":"46.4528","wind_spd":1,"wind_cdir_full":"north","wind_cdir":"N","slp":1013.5,"vis":5,"h_angle":12.9,"sunset":"18:00","dni":879.05,"dewpt":3.5,"snow":0,"uv":7.41983,"precip":0,"wind_dir":0,"sunrise":"05:10","ghi":720.88,"dhi":109.93,"aqi":null,"city_name":"District de la Riviera-Pays-d'Enhaut","weather":{"icon":"c04d","code":"804","description":"Overcast clouds"},"datetime":"2019-04-01:13","temp":17.2,"station":"E5498","elev_angle":44.65,"app_temp":17.3}],"count":1}

Status code is 200
Weather Data for city: Cook by passing PostCode as inputs is{"data":[{"wind_cdir":"SW","rh":51,"pod":"d","pres":1001,"timezone":"America\/Chicago","ob_time":"2019-04-01 13:40","country_code":"US","clouds":100,"ts":1554126000,"solar_rad":43.2,"state_code":"IL","lat":"42.0076","wind_spd":4.12,"wind_cdir_full":"southwest","slp":1026.8,"vis":10,"lon":"-87.9931","uv":1.05904,"datetime":"2019-04-01:13","h_angle":-64.3,"dewpt":-7.1,"aqi":null,"dhi":66.81,"wind_dir":220,"elev_angle":15.2,"ghi":216.11,"precip":null,"sunrise":"11:32","city_name":"Cook","weather":{"icon":"c04d","code":"804","description":"Overcast clouds"},"sunset":"00:17","temp":2,"dni":597.8,"station":"KORD","app_temp":-1.8}],"count":1}

Status code is 200
Weather Data for city: Denver by passing PostCode as inputs is{"data":[{"wind_cdir":"SSE","rh":77,"pod":"d","pres":823.3,"timezone":"America\/Denver","ob_time":"2019-04-01 13:35","country_code":"US","clouds":25,"ts":1554125700,"solar_rad":21.4,"state_code":"CO","lat":"39.7263","wind_spd":2.06,"wind_cdir_full":"south-southeast","slp":1026.9,"vis":10,"lon":"-104.8568","uv":2.03382,"datetime":"2019-04-01:13","h_angle":-77.1,"dewpt":-4.1,"aqi":null,"dhi":22.43,"wind_dir":162,"elev_angle":2.56,"ghi":21.55,"precip":null,"sunrise":"12:42","city_name":"Denver","weather":{"icon":"c02d","code":"801","description":"Few clouds"},"sunset":"01:23","temp":-0.6,"dni":177.56,"station":"AT973","app_temp":-3.1}],"count":1}

Status code is 200
Weather Data for city: Sacramento by passing PostCode as inputs is{"data":[{"wind_cdir":"SW","rh":78,"pod":"n","pres":1015.2,"timezone":"America\/Los_Angeles","ob_time":"2019-04-01 13:30","country_code":"US","clouds":45,"ts":1554125400,"solar_rad":0,"state_code":"CA","lat":"38.6383","wind_spd":1.54,"wind_cdir_full":"southwest","slp":1015.8,"vis":10,"lon":"-121.5072","uv":0,"datetime":"2019-04-01:13","h_angle":-90,"dewpt":8.5,"aqi":null,"dhi":0,"wind_dir":221,"elev_angle":-10.46,"ghi":0,"precip":null,"sunrise":"13:50","city_name":"Sacramento","weather":{"icon":"c01n","code":"800","description":"Clear sky"},"sunset":"02:29","temp":12.2,"dni":0,"station":"E9060","app_temp":12.3}],"count":1}

Status code is 200
Weather Data for city: El Paso by passing PostCode as inputs is{"data":[{"wind_cdir":"N","rh":62,"pod":"d","pres":788.4,"timezone":"America\/Denver","ob_time":"2019-04-01 13:40","country_code":"US","clouds":0,"ts":1554126000,"solar_rad":20.5,"state_code":"CO","lat":"38.876","wind_spd":2.24,"wind_cdir_full":"north","slp":1002.4,"vis":10,"lon":"-104.817","uv":2.45417,"datetime":"2019-04-01:13","h_angle":-77.1,"dewpt":-8.8,"aqi":null,"dhi":21.83,"wind_dir":9,"elev_angle":2.46,"ghi":20.48,"precip":null,"sunrise":"12:42","city_name":"El Paso","weather":{"icon":"c01d","code":"800","description":"Clear sky"},"sunset":"01:23","temp":-2.6,"dni":173.01,"station":"D3529","app_temp":-5.8}],"count":1}

PASSED: testDataLatLon(-33.86785, 151.20732, "Sydney")
PASSED: testDataLatLon(-37.814, 144.96332, "Melbourne")
PASSED: testDataLatLon(-31.95224, 115.8614, "Perth")
PASSED: testDataLatLon(-34.92866, 138.59863, "Adelaide")
PASSED: testDataLatLon(-27.46794, 153.02809, "Brisbane")
PASSED: testDataLatLon(1817, "Cheshire")
PASSED: testDataLatLon(60007, "Cook")
PASSED: testDataLatLon(80201, "Denver")
PASSED: testDataLatLon(95834, "Sacramento")
PASSED: testDataLatLon(80907, "El Paso")

===============================================
    Default test
    Tests run: 10, Failures: 0, Skips: 0
===============================================


===============================================
Default suite
Total tests run: 10, Failures: 0, Skips: 0
===============================================
